# DOTFILES ->> SYSNOMID.com

This is my dotfiles repo. 
Right now it has Neovim and ZSH configuration.

These dotfiles have a few dependencies to work.

For zsh, you will need to install oh-my-zsh, zgen, and zsh-autosuggestions like such
oh-my-zsh

	```sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"```

zgen:

	```git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"```

zsh-autosuggestions

	```git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions```


For Neovim, you will need nvm, and nodejs above version 10.12. You will also need python3 with the jedi module, as well as pynvim.
Install NVM
	```curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash```


ADD this to .zshrc/.bashrc(already added to mine)

	```export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm```


Install NodeJS 11
```     nvm install 11
	    nvm use 11
```


Use pip or pip3 to then install jedi and pynvim.



VIM-PLUG is also a major dependency, you can install it via 
	```curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim                                                  ```



I store my dotfiles using the GNU Stow software.
Install it here,
	```Debian/Ubuntu ->> sudo apt-get install stow git
	```Arch/Manjaro ->> sudo pacman -S stow git
	```RHEL derivs ->> sudo dnf install stow git
	```


